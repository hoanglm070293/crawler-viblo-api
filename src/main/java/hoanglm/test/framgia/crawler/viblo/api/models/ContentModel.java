package hoanglm.test.framgia.crawler.viblo.api.models;

import org.jsoup.select.Elements;

/**
 * Created by hoanglm on 9/22/17.
 */
public class ContentModel {
    public static Elements content;

    public static Elements getContent() {
        return content;
    }

    public static void setContent(Elements content) {
        ContentModel.content = content;
    }
}
