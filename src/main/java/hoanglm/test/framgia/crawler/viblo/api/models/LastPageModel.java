package hoanglm.test.framgia.crawler.viblo.api.models;

/**
 * Created by hoanglm on 11/6/18.
 */
public class LastPageModel {
    public static String lastPage;

    public static String getLastPage() {
        return lastPage;
    }

    public static void setLastPage(String lastPage) {
        LastPageModel.lastPage = lastPage;
    }
}
