package hoanglm.test.framgia.crawler.viblo.api.services;

import hoanglm.test.framgia.crawler.viblo.dal.entities.Content;
import hoanglm.test.framgia.crawler.viblo.dal.repositories.ContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by hoanglm on 11/6/18.
 */
@Service
public class ContentService {

    @Autowired
    private ContentRepository contentRepository;

    public Content create(Content content) {
        return contentRepository.save(content);
    }
}
