package hoanglm.test.framgia.crawler.viblo.api.services;

import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import hoanglm.test.framgia.crawler.viblo.dal.entities.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Pattern;

/**
 * Created by hoanglm on 11/6/18.
 */
@Service
public class CrawlerVibloService extends WebCrawler {

    @Autowired
    ContentService contentService;

    private static final Logger logger = LoggerFactory.getLogger(CrawlController.class);

    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg"
            + "|png|mp3|mp4|zip|gz))$");

    private static Queue queue = new LinkedList();

    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        return !FILTERS.matcher(href).matches()
                && href.startsWith("https://www.ics.uci.edu/");
    }

    @Override
    public void visit(Page page) {
        String url = page.getWebURL().getURL();
        logger.info("URL: " + url);

        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String html = htmlParseData.getHtml();
            Document doc = Jsoup.parse(html);
            Elements title = doc.select("div[class=post-feed-item]");
            for (Integer i = 0;  i < title.size(); i++) {
                boolean isJava = title.eq(i).toString().toLowerCase().indexOf("java") !=-1? true: false;
                if (!isJava) {
                    queue.add(title.eq(i).toString());
                }
            }
        }
        logger.info("Crawl done this page.");
    }

    public String crawlerContent(){
        while (true) {
            if (queue.isEmpty()) {
                break;
            } else {
                Content content = new Content();
                content.setContent(queue.remove().toString());
                contentService.create(content);
            }
        }
        return null;
    }
}
