package hoanglm.test.framgia.crawler.viblo.api.controllers;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import hoanglm.test.framgia.crawler.viblo.api.services.ContentService;
import hoanglm.test.framgia.crawler.viblo.api.services.CrawlerVibloLastPageService;
import hoanglm.test.framgia.crawler.viblo.api.services.CrawlerVibloService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by hoanglm on 11/6/18.
 */
@RestController
@RequestMapping("/crawler")
public class CrawlerVibloController {

    @Autowired
    ContentService contentService;

    @Autowired
    CrawlerVibloLastPageService crawlerVibloLastPageService;

    @Autowired
    CrawlerVibloService crawlerVibloService;

    @Value("${linkStorage}")
    private String link;

    @Value("${urlWebsite}")
    private String url;

    private static final Logger logger = LoggerFactory.getLogger(CrawlController.class);

    @PostMapping("/lastPage")
    @ResponseBody
    public String getLastPage() {
        logger.info("Starting find last page....");
        try {
            String crawlStorageFolder = link;
            int numberOfCrawlers = 1;

            CrawlConfig config = new CrawlConfig();
            config.setCrawlStorageFolder(crawlStorageFolder);
            config.setThreadMonitoringDelaySeconds(0);
            config.setThreadShutdownDelaySeconds(1);
            config.setCleanupDelaySeconds(0);

            PageFetcher pageFetcher = new PageFetcher(config);
            RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
            RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
            CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);

            String link = String.format(url);
            controller.addSeed(link);

            controller.start(CrawlerVibloLastPageService.class, numberOfCrawlers);
            return crawlerVibloLastPageService.getLastPage();
        } catch (Exception e) {
            System.out.println(e);
            logger.error("Faill: " + e);
            return null;
        }
    }

    @PostMapping("/all")
    @ResponseBody
    public String getAllContent() {
        Integer lastPage = Integer.parseInt(getLastPage());
        try {
            String crawlStorageFolder = link;
            int numberOfCrawlers = 1;

            CrawlConfig config = new CrawlConfig();
            config.setCrawlStorageFolder(crawlStorageFolder);
            config.setThreadMonitoringDelaySeconds(0);
            config.setThreadShutdownDelaySeconds(1);
            config.setCleanupDelaySeconds(0);

            PageFetcher pageFetcher = new PageFetcher(config);
            RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
            RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
            CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);

            for(int i = 0; i < lastPage; i++) {
                Integer page = i + 1;
                String link = String.format("%s/?page=%d", url, page);
                controller.addSeed(link);
            }

            controller.start(CrawlerVibloService.class, numberOfCrawlers);
            return crawlerVibloService.crawlerContent();
        } catch (Exception e) {
            logger.error("Faill: " + e);
            return null;
        }
    }
}
